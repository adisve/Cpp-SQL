#include <stdlib.h>
#include <string>
#include <iostream>
#include <User.h>
#include <Connector.h>


/** The constructor accepts a User object along with the name of the database   **/
Connector::Connector(User user, std::string database){
    this->driver = get_driver_instance();
    this->conn_user = user.getName();
    this->conn_pass = user.getPass();
    try{
        this->conn = driver->connect("tcp://localhost:3306", conn_user, conn_pass);   
    }catch(...){
        std::cout << "Connection error occurred" << std::endl;
    }
};
bool Connector::get_connection(){
    return conn->isValid();
};

void Connector::query(std::string sqlquery){
    res = stmt->executeQuery(sqlquery);
    while(res->next()){
        std::cout << "id = " << res->getInt(1);
        std::cout << ", label = '" << res->getString("label") << "'" << std::endl;
    }
    delete res;
}
void Connector::freeObjects(){
    delete stmt;
    delete conn;
    delete res;
}

