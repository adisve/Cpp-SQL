#include <stdlib.h>
#include <iostream>
#include <User.h>
#include <Connector.h>

int main(void)
{

  std::string username = "guest";
  std::string password = "password";
  std::string database = "world";

  try{
    /* Attempt to instantiate User and Connector object */

    User _user = User(username, password);
    Connector _connector = Connector(_user, database);

  }catch (sql::SQLException &e) {

    std::cout << "# ERR: SQLException in " << __FILE__;
    std::cout << "(" << __FUNCTION__ << ") on line " 
      << __LINE__ << std::endl;
    std::cout << "# ERR: " << e.what();
    std::cout << " (MySQL error code: " << e.getErrorCode();
    std::cout << ", SQLState: " << e.getSQLState() << " )" << std::endl;
  }

  return EXIT_SUCCESS;
}
