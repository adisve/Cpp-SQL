#ifndef CONNECTOR_H
#define CONNECTOR_H

#include <stdlib.h>
#include <string>
#include <iostream>
#include <connection.h>
#include <resultset.h>
#include <statement.h>
#include <resultset.h>
#include <driver.h>
#include <xdevapi.h>
#include <User.h>
#include "mysql_connection.h"
using namespace sql;

class Connector
{
    /**  Class for handling the connection to MySQL server  **/
private:
    Driver          *driver;
    ResultSet       *res;
    Statement       *stmt;
    Connection      *conn;
    std::string     conn_user;
    std::string     conn_pass;
    
public:
    /** The constructor accepts a User object along with the name of the database   **/
    Connector();

    Connector(User user, std::string database);
    
    bool get_connection();

    void query(std::string sqlquery);
    
    void freeObjects();
};


#endif