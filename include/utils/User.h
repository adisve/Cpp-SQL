#ifndef USER_H
#define USER_H

#include <string>

class User
{
private:
    const   std::string name;
    const   std::string password;
public:
    User();

    User(std::string _username, std::string _password);

    std::string getName();

    std::string getPass();
};
#endif